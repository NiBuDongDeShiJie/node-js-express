var express = require('express');
var router = express.Router();

var mysql = require("mysql");
var db = require('../db.js');
var pool = mysql.createPool(db.mysql);

/**
 * 路由
 */
router.get('/abc', function(req, res, next) {

  pool.getConnection(function (err, connection) {
    let params = req.query || req.params; //前端传的参数（暂时写这里，在这个例子中没用）
    let sql = 'select * from test';
    connection.query(sql, function (err, result) {
      // 模板渲染
      res.render('index', {content: result});
      // 释放链接
      connection.release();
    })
  })
});

module.exports = router;
