# node-js-express

#### 介绍
通过node-js的express web框架搭建一个web应用的例子

#### 软件架构
软件架构说明


#### 本例子的安装教程

1. 环境 node v14+
2. npm install express -g // 安装express 最新版本
3. npm install -g express-generator // 安装express 命令工具
4. express --version // 查看版本
5. mkdir node-js-express && cd node-js-express
6. express node-js-express //  创建工程 生成目录结构
7. npm install // 安装依赖，在 node-js-express目录中执行
8. npm start // 在 node-js-express目录中执行启动，实际上是执行的 node ./bin/www 在 package.json 中配置
9. routes目录相当于controller其他的目录自己可以创建，比较灵活
10. 在 routes/index.js演示了基础的mysql操作
11. 仅记录学习过程，仅供参考。